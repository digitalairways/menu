(function() {
    var factory = function (gibson) {
        const TYPE = ["button","divider","subheader","content"]
        var widgetName = "menu"
        gibson.export.renderer = {
            html: function(token) {

                var data = token.data

                let menu = new Wmenu(token.widgetID, data)

                menu.init()
                menu.setMenu();


                return menu.html.outerHTML;
            }
        };

        gibson.export.listeners = function() {
            Wmenu.listeners(gibson.widgets[widgetName].tokens)
            //console.log(instances)
        };
         gibson.export.unbindListeners = function() {
            Wmenu.unbindListeners(gibson.widgets[widgetName].tokens)
        };

    };

    factory(window.gibson)

})();


