var Wmenu = (function() {

    const NAME = "menu"


    function doNotNavigate(e) {
        let url = e.target.closest('a').getAttribute('href')
        if (url) {
            let params = geturlparams(url)
            if (params.markdown) {
                gibson.goToGibsonUrl(params.markdown, (params.record || ""), params);
            }
            e.preventDefault();
            return false;
        }
    }

    function goBack(e) {
        e.preventDefault();
        window.history.back()
    }

    function generateIcon(element){
        let i = document.createElement("i")
        if(element["icon"].startsWith("fa-")){
            i.classList.add("fa")
            i.classList.add(...element["icon"].split(" "))
        } else {
            i.classList.add("material-icons")
            i.textContent = element["icon"]
        }
        return i;
    }
    var Wmenu = function(id, data) {
        this.id = id
        this.width = data.width || "auto"
        this.height = data.height || "auto"
        this.elements = Array.isArray(data.element) ? data.element : [data.element]
        this.color = data.color
        this.type = data.type || "burger"
        this.type = this.type.charAt(0).toUpperCase() + this.type.slice(1)
        this.options = data.options || {}
    }

    Wmenu.callInnerWidgetsListeners = function(widgetID) {
        gibson.callInnerWidgetsListeners(widgetID);
    }
    Wmenu.prototype.init = function() {
        this.html = document.createElement("div")
        this.html.setAttribute("widget-id", this.id)
        this.html.setAttribute("name", NAME)
        this.html.classList.add(`${NAME}-container`)
        this.html.classList.add('valign-wrapper')
        this.html.classList.add(`${NAME}-${this.type}`)
        this.html.style.width = this.width
        this.html.style.height = this.height
        return this;
    }
    Wmenu.prototype.setMenu = function() {
        this.menu = new Wmenu[this.type](this.html, this.id, this.color, this.options).init()
        for (var i in this.elements) {
            this.menu.addElement(this.elements[i])
        }
        return this
    }
    Wmenu.listeners = function(tokens) {
        Wmenu.Burger.listeners(tokens)
        Wmenu.Navbar.listeners(tokens)
        Wmenu.Float.listeners(tokens)
    }
    Wmenu.unbindListeners = function(tokens) {
        Wmenu.Burger.unbindListeners(tokens)
        Wmenu.Navbar.unbindListeners(tokens)
        Wmenu.Float.unbindListeners(tokens)
    }
    Wmenu.Navbar = (function() {
        var Navbar = function(container, id, color = "white") {
            this.container = container
            this.id = id
            this.color = color
        }
        Navbar.prototype.init = function() {
            this.html = document.createElement("ul")
            this.container.append(this.html)
            return this
        }
        Navbar.prototype.addElement = function(element) {
            this.html.append(this.getElement(element))
        }
        Navbar.prototype.getElement = function(element) {
            let li = document.createElement("li")
            li.classList.add("right")
            let a = document.createElement("a")

            a.href = element["link"] || "#"

            a.classList.add("valign-wrapper")

            if (element.doNotNavigate === "true") {
                a.classList.add("doNotNavigate")
            } else if (element.goBack === "true") {
                a.classList.add("goBack")
            }

            if (element.hasOwnProperty("icon")) {
                let i = generateIcon(element);
                if (element.hasOwnProperty("text") && element["text"] !== "") {
                    a.classList.add("tooltipped")
                    a.setAttribute("data-tooltip", element["text"])
                    a.setAttribute("data-position", "bottom")
                }
                a.append(i)
            } else {
                a.textContent = element["text"]
            }
            a.style.color = element["color"] || "white"
            li.append(a)
            return li
        }
        Navbar.listeners = function(tokens) {
            let elems = document.querySelectorAll(`.${NAME}-container .tooltipped`)
            M.Tooltip.init(elems, {})

            let goBacks = document.querySelectorAll(`.${NAME}-container .goBack`)
            for (var i = 0; i < goBacks.length; i++) {
                goBacks[i].addEventListener('click', goBack, false);
            }

            let doNotNavigates = document.querySelectorAll(`.${NAME}-container .doNotNavigate`)
            for (var i = 0; i < doNotNavigates.length; i++) {
                doNotNavigates[i].addEventListener('click', doNotNavigate, false);
            }
        }
        Navbar.unbindListeners = function(tokens) {

        }

        return Navbar
    })();
    Wmenu.Float = (function() {
        var Float = function(container, id, color, options) {
            this.container = container;
            this.id = id;
            this.color = color;
            this.fixed = options.fixed !== "false";
            this.align = options.align || "right";
        }
        Float.prototype.init = function() {
            const button = document.createElement("a");
            button.classList.add("btn-floating");
            button.classList.add("btn-large");
            button.style["background-color"] = this.color || "#2196f3";
            button.style.cursor = "pointer";

            const button_icon= generateIcon({icon : "menu"});
            button.style.color = "white";
            button.append(button_icon);

            this.html = document.createElement("ul");

            const subcontainer = document.createElement("div");

            subcontainer.classList.add("fixed-action-btn");
            if (!this.fixed) {
                subcontainer.classList.add("menu-float-notfixed");
            }
            if (this.align === "right") {
                this.container.classList.add("right");
            } else {
                this.container.classList.add("left");
            }
            subcontainer.id = this.id;
            subcontainer.append(button);
            subcontainer.append(this.html);
            this.container.append(subcontainer);
            return this
        }
        Float.prototype.addElement = function(element) {
            this.html.append(this.getElement(element))
        }
        Float.prototype.getElement = function(element) {
            let li = document.createElement("li")
            let a = document.createElement("a")

            a.href = element["link"] || "#";
            a.classList.add("btn-floating");
            a.classList.add("btn-large");

            if (element.hasOwnProperty("icon")) {
                let i = generateIcon(element);
                if (element.hasOwnProperty("text") && element["text"] !== "") {
                    a.classList.add("tooltipped")
                    a.setAttribute("data-tooltip", element["text"])
                }
                a.append(i)
            }
            if (element.doNotNavigate === "true") {
                a.classList.add("doNotNavigate")
            } else if (element.goBack === "true") {
                a.classList.add("goBack");
            }
            a.style["background-color"] = element["color"] || "#2196f3";
            li.append(a);
            return li
        }
        Float.listeners = function(tokens) {
            let elems = document.querySelectorAll(`.${NAME}-container .fixed-action-btn`)
            for (var i = 0; i < elems.length; i++) {
                let id = elems[i].id
                let direction = tokens[id].data.direction || 'top'
                let tooltipDirection = tokens[id].data.tooltipsDirection || 'bottom'

                M.FloatingActionButton.init(elems[i], {
                    direction: direction
                })
                let tooltips = elems[i].querySelectorAll(`.tooltipped`)
                M.Tooltip.init(tooltips, {
                    position: tooltipDirection
                })
            }
            let goBacks = document.querySelectorAll(`.${NAME}-container .goBack`)
            for (var i = 0; i < goBacks.length; i++) {
                goBacks[i].addEventListener('click', goBack, false);
            }

            let doNotNavigates = document.querySelectorAll(`.${NAME}-container .doNotNavigate`)
            for (var i = 0; i < doNotNavigates.length; i++) {
                doNotNavigates[i].addEventListener('click', doNotNavigate, false);
            }
        }

        Float.unbindListeners = function(tokens) {

        }
        return Float
    })();
    Wmenu.Burger = (function() {
        var Burger = function(container, id, color, options) {
            this.container = container;
            this.id = id;
            this.color = color;
            this.margin = options.margin;
            this.size = options.size;
        }
        Burger.prototype.init = function() {
            const button = document.createElement("a");
            button.setAttribute("data-target", this.id);
            button.classList.add("sidenav-trigger");
            button.classList.add("sidenav-close");
            button.classList.add("valign-wrapper");
            button.style.cursor = "pointer";
            button.style.width = "100%";
            button.style.height = "100%";


            const button_icon = document.createElement("i")
            if (this.margin) button_icon.style.margin = this.margin;
            if (this.size) { button_icon.classList.add(this.size) };
            button_icon.textContent = "menu";
            button_icon.classList.add("material-icons");
            button.style.color = this.color;
            button.append(button_icon);

            this.html = document.createElement("ul");
            this.html.classList.add("sidenav");
            this.html.classList.add(NAME);
            this.html.id = this.id;
            this.container.append(button);

            //remove old menu before adding a new one
            // ! The widget works with only one burger menu !
            let elems = document.querySelectorAll('.sidenav.' + NAME);
            elems.forEach((j) => {
                var instance = M.Sidenav.getInstance(j);
                if (instance) instance.destroy();
                try {
                    j.remove();
                } catch (e) {}
            })

            document.getElementsByTagName('header')[0].append(this.html);
            return this
        }
        Burger.prototype.addElement = function(element) {
            this.html.append(this.getElement(element))
        }
        Burger.prototype.getElement = function(element) {

            const TYPE = ["button", "divider", "subheader", "content"]

            let li = document.createElement("li");
            let a, div;
            if (TYPE.indexOf(element.type) === -1) element.type = "content";
            switch (element.type) {
                case 'button':
                    a = document.createElement("a");
                    a.classList.add("waves-effect");
                    a.href = element["link"] || "#";
                    a.style.color = element["color"] || "inherit";
                    div = document.createElement("div");
                    div.textContent = element["text"];
                    a.prepend(div);
                    if (element.doNotNavigate === "true") {
                        a.classList.add("doNotNavigate");
                    } else if (element.goBack === "true") {
                        a.classList.add("goBack");
                    }
                    if (element.hasOwnProperty("icon")) {
                        let i = generateIcon(element);
                        a.prepend(i);
                    }
                    li.append(a);
                    break;
                case 'divider':
                    div = document.createElement("div");
                    div.classList.add("divider");
                    li.append(div);
                    break;
                case 'subheader':
                    a = document.createElement("a");
                    a.classList.add("subheader");
                    a.textContent = element["text"];
                    li.append(a);
                    break;
                case 'content':
                    li.innerHTML = gibson.renderer(element["content"] || "")
                    break;
            }
            return li;
        }
        Burger.listeners = function(tokens) {
            let elems = document.querySelectorAll('.sidenav.' + NAME);
            let instances = [];
            elems.forEach((elem) => {
                var token = tokens[elem.id];
                let options = {}
                if (token.data.options) {
                    if (token.data.options.align) {
                        options.edge = token.data.options.align
                    }
                }
                instances.push(M.Sidenav.init(elem, options));
            })
            var containers = document.querySelectorAll(`.${NAME}-container`)
            for (var i = 0; i < containers.length; i++) {
                let parent = containers[i].parentNode;
                parent.style.width = containers[i].style.width;
                parent.style.height = containers[i].style.height;
            }
            for (var j in instances) {
                var instance = instances[j];
                var token = tokens[instance.el.id];
                if (token) {
                    if (token.data.overlay === "none") {
                        instance._overlay.classList.add("menu-hidden-overlay");

                        var timeout = false;
                        var canClose = true;
                        let check_isOpen = function(event) {
                            //on opening, false, on opened true
                            canClose = instance.isOpen;
                        }
                        let close_instance = function(event) {
                            //on opening, false, on opened true
                            if (canClose) {
                                timeout = setTimeout(function() {
                                    instance.close()
                                }, 100);
                            }
                            canClose = true;
                        }
                        let stop_close_instance = function(event) {
                            if (timeout) {
                                clearTimeout(timeout);
                                timeout = false;
                            }
                        }

                        window.addEventListener("click", close_instance, false);
                        instance.el.addEventListener("click", stop_close_instance, false);
                        let buttons = document.querySelectorAll(`a[data-target=${instance.el.id}]`);
                        for (var i = 0; i < buttons.length; i++) {
                            buttons[i].addEventListener("click", check_isOpen, false);
                        }
                    }

                    let doNotNavigates = instance.el.querySelectorAll(`.doNotNavigate`);
                    for (var i = 0; i < doNotNavigates.length; i++) {
                        doNotNavigates[i].addEventListener('click', doNotNavigate, false);
                    }

                    if (token.data.top) {
                        instance._overlay.style.setProperty("top", token.data.top);
                        instance.el.style.setProperty("top", token.data.top);
                        instance.el.style.setProperty("height", "calc(100% - " + token.data.top + ")", "important");
                    }
                    Wmenu.callInnerWidgetsListeners(token.widgetID)

                }
            }
        }
        Burger.unbindListeners = function(tokens) {
            let elems = document.querySelectorAll('.sidenav.' + NAME);
            elems.forEach((j) => {
                var instance = M.Sidenav.getInstance(j);
                if (instance) instance.destroy();
            })
        }
        return Burger;
    })();

    return Wmenu;
})();